package main

// Construyendo tu primer aplicación en equipo en GCP
// Mayo 26, 2020
// Por Ronney Espinosa Hernández
// Correo: ronneyalberto.espinosa.hernan.next@bbva.com

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", indexHandler)
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		// Manejar error en caso de que puerto este utilizado por ejemplo.
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	fmt.Fprint(w, "Taller Ninja de GCP!\n")
    fmt.Fprint(w, "Versión: 1.0\n")
    fmt.Fprint(w, "Versión: 2   .0\n")
    fmt.Fprint(w, "Versión: 3.0 yosimar.arellano@bbva.com\n")
}
